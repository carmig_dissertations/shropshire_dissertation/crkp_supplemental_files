### INSTALL AND LOAD PACKAGES ################################

pacman::p_load(lars, 
               pacman, 
               rio, 
               tidyverse, 
               caret, 
               glmnet, 
               summarytools, 
               broom, 
               RVAideMemoire,
               data.table, 
               tictoc,
               DMwR,
               ROCR)

### LOAD IN DATSET FROM analysis_v2

setwd('~/Documents/Active_projects/HTX_Kpn/clinical_data_analysis/')

df <- import("vsurf_variable_dataset.csv") %>%
        print()

### Remove uninformative variables
#bad_vars <- c("carmig_id")
#df <- select(df, -bad_vars)

# Summary of all variables merged
SummaryData1 <- dfSummary(df, plain.ascii = FALSE, style = "grid", rows=3:101,
                          graph.magnif = 0.75, valid.col = FALSE, tmp.img.dir = "/tmp") 
print(SummaryData1)

# Change variable names that may be an issue with downstream functions

colnames(df)[8] <- "ykfA"
colnames(df)[15] <- "intA"
colnames(df)[20] <- "vapC"
colnames(df)[23] <- "cat"

# Set random seed
set.seed(333)

# factor variables
#factor_vars = c("htx", "anatomical_source", "invasive_CRE", "mortality_30_day", "pdxI", 
#                "group_5226", "group_3813", "group_5004", "group_4837", "group_4228", 
#                "group_2657", "group_4009", "group_4711", "xerC", "vapC", "group_4926", 
#                "group_5123", "group_2025", "group_314", "group_4478", "group_3403", 
#                "group_5205", "group_2612", "group_4622", "group_7741", "group_431", 
#                "group_6917", "oqxB19")

df[3:27] <- lapply(df[ ,3:27], factor)
df$mortality_30_day <- factor(df$mortality_30_day)

#htx_df <- df %>% filter(htx == "Yes")
#htx_df$htx <- NULL

# Check for variables that contribute no variance.

#nzv <- nearZeroVar(df[,-1])
#df <- df[ ,-nzv]

# Split data into train and test sets
set.seed(1219)

inTrain <- createDataPartition(y = df$mortality_30_day, p = 0.5, list=FALSE)
training <- df[inTrain,] # Sample 50% of data
testing  <- df[-inTrain,]     # Use remaining cases

table(training$mortality_30_day) 
table(testing$mortality_30_day)

# Use synthetitc minority oversampling technique on training set

model_df_balance <- SMOTE(mortality_30_day ~ ., training, perc.over = 600,perc.under=200)
table(model_df_balance$mortality_30_day)

# RANDOM FOREST ON TRAINING DATA ###########################

# Cross validation on training set: create 10 random partitioning of the training set and repeat 10 times
# Define parameters for the random forest
set.seed(4721)
control <- trainControl(
        method  = "repeatedcv",  # Repeated cross-validation
        number  = 10,            # Number of folds
        repeats = 10,             # Number of sets of folds
        search  = "random",      # Max number of tuning parameters
        allowParallel = TRUE     # Allow parallel processing
)

# Train random forest on training data (can take a while)
tic("Random forest")
fit_rf <- train(
        mortality_30_day ~ .,
        data = model_df_balance,         # Use training data
        method = "rf",        # Use random forests
        metric = "Accuracy",  # Use accuracy as criterion
        tuneLength = 15,      # Number of levels for parameters
        ntree = 300,          # Number of trees
        trControl = control   # Link to parameters
)
toc() # 1 min

# Show processing summary
fit_rf

# Plot accuracy by number of predictors
fit_rf %>% plot()

# Accuracy of model with training data
fit_rf$finalModel

# Plot error by number of trees; Red is error for "High,"
# green is error for "Low," and black is error or "OOB,"
# or "out of bag" (i.e., the probability that any given 
# prediction is not correct within the test data, or the 
# overall accuracy)
fit_rf$finalModel %>% plot()

# Predict training data
mortality_p <- fit_rf %>%        # "predicted"
        predict(newdata = training)  # Use train data

# Accuracy of model on training data
table(
        actualclass = training$mortality_30_day, 
        predictedclass = mortality_p
) %>%
        confusionMatrix() %>%
        print()  # 1.0 accuracy on full training data of random forest model - probably overfitting

# RANDOM FOREST ON TEST DATA ###############################
# Predict testing data
mortality_p <- fit_rf %>%        # "predicted"
        predict(newdata = testing)  # Use test data

# Accuracy of model on test data
table(
        actualclass = testing$mortality_30_day, 
        predictedclass = mortality_p
) %>%
        confusionMatrix() %>%
        print() # 78.57% accuracy of random forest model on testing data

# stochastic gradient boosting 

# Cross validation on training set: create 10 random partitioning of the training set and repeat 10 times
# Define parameters for the random forest

fitControl <- trainControl(
        method = "repeatedcv",
        number = 10,
        repeats = 10,
        allowParallel = TRUE)


set.seed(12345)
tic("GBM")
gbmFit1 <- train(mortality_30_day ~ ., data = model_df_balance, 
                 method = "gbm", 
                 trControl = fitControl)
toc()

# Show processing summary
gbmFit1

# Plot accuracy by boosting iterations
gbmFit1 %>% plot()

# Accuracy of model with training data
gbmFit1$finalModel

# Plot error by number of trees; Red is error for "High,"
# green is error for "Low," and black is error or "OOB,"
# or "out of bag" (i.e., the probability that any given 
# prediction is not correct within the test data, or the 
# overall accuracy)
gbmFit1$finalModel %>% plot()

# Predict training data
mortality_p <- gbmFit1 %>%        # "predicted"
        predict(newdata = training)  # Use train data

# Accuracy of model on test data
table(
        actualclass = training$mortality_30_day, 
        predictedclass = mortality_p
) %>%
        confusionMatrix() %>%
        print()  # 91.3% accuracy of GBM model on full training data 

# GBM ON TEST DATA ###############################

# Predict test set
mortality_p <- gbmFit1%>%       # "predicted"
        predict(newdata = testing)  # Use test data

# Accuracy of model on test data
table(
        actualclass = testing$mortality_30_day, 
        predictedclass = mortality_p
) %>%
        confusionMatrix() %>%
        print()  

# Logistic Regression ###############################
set.seed(421)
model2 <- glm(mortality_30_day ~ ., data=model_df_balance, family = binomial)
summary(model2)

## Predict the Values
predict <- predict(model2, testing, type = 'response')

## Create Confusion Matrix
table(testing$mortality_30_day, predict > 0.5)

ROCRpred <- prediction(predict, testing$mortality_30_day)
ROCRperf <- performance(ROCRpred, 'tpr','fpr')
plot(ROCRperf, colorize = TRUE, text.adj = c(-0.2,1.7))

### Unregister unused backend parallel cluster

unregister <- function() {
        env <- foreach:::.foreachGlobals
        rm(list=ls(name=env), pos=env)
}

# CLEAN UP #################################################

# Clear data
rm(list = ls())  # Removes all objects from environment

# Clear packages
p_unload(all)  # Remove all contributed packages

# Clear plots
graphics.off()  # Clears plots, closes all graphics devices

# Clear console
cat("\014")  # Mimics ctrl+L

